module.exports = function(app, apiRoutes){
    var mongoose = require('mongoose');
    var userHelper = require('../models/userHelper');
    var User = require('../models/User');


    function create(req, res){
        userHelper.create({
            username      : req.body.username,
            password      : req.body.password,
            last_name     : req.body.last_name,
            email     : req.body.email,
            name     : req.body.name,
            type : req.body.type,
            metadata : req.body.metadata || {}
           // _credential   : req.body.credential
           // _Role         : req.body.role
        }, function(err, usuario){
            res.send(usuario);
        });
    }



    function update(req, res){
         var data = {};
         var REQ = req.body || req.params;

         !REQ.name || (data.name = REQ.name);             
         !REQ.last_name|| (data.last_name = REQ.last_name);             
         !REQ.email || (data.email = REQ.email);                          
         !REQ.metadata || (data.metadata = REQ.metadata);             



         /*if(REQ._route)
         {
           data._route = [];

             for(x in REQ._route)
                 data._route.push(mongoose.Types.ObjectId(REQ._route[x]));
          }
          */


         data = { $set : data };          

         userHelper.update({ _id : mongoose.Types.ObjectId(req.params.id) }, data, function(err, rs){
                if(rs)
                    res.json(rs);
         });

    }

    function remove(req, res){
        userHelper.remove(req.params.id, function(err, user){
            if(!err){
                user.remove();
                res.status(200)
                res.end();
            }
        })
    }

    function users(req, res){

        var Role = require("../models/roles");

        User.find()
        .populate("_role")
        .exec(function(err, users){
            if(!err){
                res.send(users);
            }
        });

    }

    function user(req, res){

        var Role = require("../models/roles");

        User
        .findOne( mongoose.Types.ObjectId(req.params.id) )
        .populate("_role")
        .exec(function(err, rs){

            if(rs)
                res.json(rs);
            else
                res.json(err);

        })
    }

    function login(req, res){

        //se modifico este metodo para efectos de mejor arquitectura de la app


            if (!req.body.username) {
                res.status(400).send({err : 'debe especificar un usuario'});
                return;
            }

            if (!req.body.password) {
                
                res.status(400).send({err : 'debe especificar un password'});
                return;
            }

  //dev-core
            //validamos usuario
            var jwt = require('jsonwebtoken');
            var UserSchema = User;
            var Role = require('../models/roles');

         UserSchema.findOne({username : req.body.username})
         .populate("_role ")
         .exec(function(err, user){

            if(!user){
                    res.json({err : 'Usuario o clave incorrectos'}, 401);
                    return;
             }
  

              if(user.auth(req.body.password))
                 {

                    user.password = null;

                    var token = jwt.sign(user, app.get('secret'), {
                        expiresIn: 43200 // 24 horas (suficientes para una jornada laboral)
                      });


                    userHelper.createSession({token : token, user : user }, function(err, userToken){
                        
                      

                          res.json({token:token, user : user});




                    });  


                }
                else
                res.json({err: 'Usuario o clave incorrectos'}, 401);


        });

         

    }

    function exists(req, res){

        User.exists(req.params.username.toLowerCase(), function(err, rs){

            console.log(rs);

           rs = rs === 0 ? -1 : rs;

           res.json({ exists : rs});


        }) 
    }

    //Routes
    apiRoutes.get('/user', users);
    apiRoutes.get('/user/:id', user);
    app.get('/user/exists/:username', exists);
    apiRoutes.post("/user", create);
    app.post("/api/login", login);
    apiRoutes.put("/user/:id", update);
    apiRoutes.delete("/user/:id", remove);

    return this;
}