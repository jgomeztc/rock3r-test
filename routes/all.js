module.exports = exports = function(app, apiRoutes, io){

	var config = require("../config");
	var fs = require("fs");

	app.get("/", function(req, res){

			 var img = fs.readFileSync(process.env.PWD + '/logo.jpg');
		     res.writeHead(200, {'Content-Type': 'image/gif' });
		     res.end(img, 'binary');
			
	});

	


	apiRoutes.get('/', function(req, res) {
			res.json({ message: config.app_name });
	});


	  
	var fs = require('fs');
	var files = fs.readdirSync('./routes');

	console.log(files)

		for (x in files)
		 if(!files[x].match('gitignore|base|config|zip|json|all|Socket|save') && files[x].match('.js'))				 
   		      require('./' + files[x])(app, apiRoutes, io);

}