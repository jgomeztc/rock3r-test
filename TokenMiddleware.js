module.exports = function(app, apiRoutes, io){
	apiRoutes.use(function(req, res, next){
	        // check header or url parameters or post parameters for token
	        var token = req.body.token || req.query.token || req.headers['x-rtc-auth'];
	        // decode token
	        if(token){
	          // verifies secret and checks exp
				jwt.verify(token, app.get("secret"), function(err, decoded) {      
				 	var Session = require("./models/session");

					if(err){
						return res.status(401)
							   .json({ success: false, message: 'Failed to authenticate token.' }); 
					}
				    
				    Session.find({token : token}, function(err, rs){
				        if(!err){ 
				                // if everything is good, save to request for use in other routes
				                if(rs.length > 0){ 
				                  req.decoded = decoded;    
				                  next();
				                }else{
				                	res.status(401)
				                	.json({ success : false, message : 'invalid token'});
				               	}
				        }
				    }) 
				});
	        }else{
				// if there is no token
				// return an error
				return res.status(403).send({ 
				  success: false, 
				  message: 'No token provided.' 
				});
	        }
	});

	app.use(function (err, req, res, next) {
	    res.status(err.status || 500);
	    res.send({message:"error"}, 500);
	    console.log(err);

	});

	apiRoutes.use(function (err, req, res, next) {
	    res.status(err.status || 500);
	    res.send({message:"error"}, 500);
	    console.log(err);
	});	
}
