/*
BATMAN REST API CORE BY GOMOSOFT
*/

var express = require("express");
var app = express();
var http = require('http').Server(app);
var config = require("./config");
var mongoose = require("mongoose");
var bodyParser = require('body-parser')
var cors = require('cors');
var jwt = require('jsonwebtoken');
var morgan = require('morgan');
var cluster = require('cluster');
var cores = require('os').cpus().length;  //numero de cpus

app.use(cors());
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.set("secret", config.secret);
process.env.PWD = process.cwd() || process.env.PWD;

var apiRoutes = express.Router();

// Sockets envitonment
var io = require("socket.io")(http);

io.on('connection', function(socket){
    console.log("new connection");
});

mongoose.connection.on('open', function(ref){
    console.log('Connected to Mongo');

   //starting controllers 
    require("./controllers/all")(app, apiRoutes, io); 
    app.use("/api", apiRoutes);

    http.listen(config.appPort, function(){
        console.log("app listen on " + config.appPort);
    }); 
});

mongoose.connection.on('error', function(err){
    console.log('Connection failed');
    console.log(err);
    return console.log(err.message);
});

try{
    //nos conectamos a la base de datos  
    mongoose.connect( config.dburl );
    console.log('Starting connection: ' + config.dburl + ', waiting...');

}catch(err) {
  console.log('Connection failed: ' + config.dburl);
}