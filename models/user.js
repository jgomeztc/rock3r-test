var crypto = require('crypto');

var base_path = process.env.PWD;

//var config = require('./config');
var config = require(base_path + '/config.js');
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// Load required packages
var timestamps = require('mongoose-timestamp');
var metadata = require('./plugins/metadata');

var _Schema = new Schema({
	  username : { type : String, trim : true, unique : true, lowercase : true},
	  password : {type: String, required : true},
	  name : { type : String, trim : true},
	  last_name : { type : String, trim : true},
	  full_name : { type : String, trim : true},
	  type : { type : String, trim : true},
	  email : { type : String, trim : true},
	  active : { type : Boolean, default : true},
	  _role : [{ type : Schema.Types.ObjectId , ref : 'Role'}],
	  _route : { type : Schema.Types.ObjectId , ref : 'Route'},
	  _stop : { type : Schema.Types.ObjectId , ref : 'Stop'},
	  _responsible : { type : Schema.Types.ObjectId , ref : 'User'}
});

//antes de salvar la clave la hasheamos 

_Schema.pre('save', function (next) {

    this.full_name = (this.name || '') + ' ' + (this.last_name  || '');
  
    next();
  
});



_Schema.statics.asociate_route = function(id, module, callback){

	if(!callback)
	{
		console.log("callback is required in this method");
		return;
	}


	this.findById(mongoose.Types.ObjectId(id), function(err, user){

		console.log(user)


		 if(user)
		 {


			      user._route = mongoose.Types.ObjectId(module);
			      user.save(callback);
			

		 }else
		   callback({err: "not records found"}, null);


	});


}

_Schema.statics.exists = function(username, callback){


	this.find({ username : username}, function(err, rs){

	            callback(err, rs.length);


	})


}


_Schema.statics.setup = function(res){

	var data = require("../setup").user;

	data.password = require('../helpers/crypto-util')(data.password);

	var _self = this;

	var Role = require("./roles");

  this.remove({}, function(){

  	Role.findOne({name:"super"}, function(err, rs){

		data._role = [];
		data._role.push(mongoose.Types.ObjectId(rs._id));

	  	console.log(rs)


		if(!err)
		{

	  _self.collection.save(data, function(err, rs){


		    if(!err)
		    	console.log("Usuario iniciado correctamente ... \n");
		    else
		    	console.log(err);

	   });

		}
		console.log(err)

	})

  })

	



}

//conservamos un metodo en el modelo que nos autentique el usuario

_Schema.methods.auth = function(password, callback){
	      

             var res = true;


            if(require('../helpers/crypto-util')(password) !== this.password)
               res = false;


             if(callback)
             	callback(res);
             else
                return res;
        

}

//add plugins
_Schema.plugin(metadata);
_Schema.plugin(timestamps);

module.exports = mongoose.model('User', _Schema);


