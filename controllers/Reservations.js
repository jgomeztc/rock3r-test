module.exports = function(app, apiRoutes, io){
    var mongoose = require('mongoose');
    var entity = require('../models/reservations');
    var entity_name = "reservations";

    function get(req, res){
        var REQ = req.params; 

       entity.find().populate("_route").exec(function(err, rs){
            if(!err){
                res.json(rs);

            }else{
                res.json(err);
            }
       });
    }

    function post(req, res){
        var data = {};
        var REQ = req.body || req.params;

        !REQ.data || (data.data = REQ.data);             
        !REQ._route || (data._route = mongoose.Types.ObjectId(REQ._route));       

        var entity = new Model(data);
        
        entity.save(function(err, rs){
            if(rs){
                res.json(rs);
            }else{
                res.json(err);
            }
        });
    }

    function update(req, res){
        var data = {};
        var REQ = req.body || req.params;

        !REQ.data || (data.data = REQ.data);             
        !REQ._route || (data._route = mongoose.Types.ObjectId(REQ._route));   

        data = { $set : data };        

        entity.update({ _id : mongoose.Types.ObjectId(req.params.id) }, data,function(err, rs){
            if(rs){
                res.json(rs);
            }
        });
    }

    function remove(req, res){
        entity.remove({_id : mongoose.Types.ObjectId(req.params.id)}, function(err, rs){
            if(!err){
                res.json(rs);            
            }else{
                res.status(500).json(err);
            }
        });
    }

    apiRoutes.get("/" + entity_name, get);
    apiRoutes.post("/" + entity_name, post);
    apiRoutes.put("/" + entity_name + "/:id", update);
    apiRoutes.delete("/" + entity_name + "/:id", remove);

    return this;
}