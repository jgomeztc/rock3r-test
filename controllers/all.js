module.exports = exports = function(app, apiRoutes, io){

	var config = require("../config");
	var fs = require("fs");

	app.get("/", function(req, res){

			 var img = fs.readFileSync(process.env.PWD + '/logo.jpg');
		     res.writeHead(200, {'Content-Type': 'image/gif' });
		     res.end(img, 'binary');
			
	});

	// this test an email

	app.get("/mailer-test", function(req, res){
		var mailer = require('../helpers/mailer');

		//configuracion de email
		var settings = {
			FROM : 'BATMAN',
			TO : 'info@gomosoft.com',
			SUBJECT : 'BATMAN MAILER'	
		} 

		//datos del template
		var data = {
			name : {
				first : 'Batman',
				last : 'Raise'
			}
		}


		mailer.send('newsletter/index.ejs', settings, data, function(_err, info){
			if(_err){throw _err}
				console.log(info);
		})
	});




	app.get("/resetall", function(req, res){

     var Role = require("../models/roles");
     var User = require("../models/user");

    
     var message = "";

 	 Role.setup(function(err, rs){

 	 	if(err)
 	 	{
 	 		 console.log("error setup");
 	         res.status(500).json({message : "err"});

 	 		 return;
 	 	}
 	 

 	     message =+ "Roles inciados correctamente...";


 	     User.setup(res);
 	     message =+ "Usuarios inciados correctamente...";


 	      res.json({message : "ok"});


 	 });
 
	

	})


	apiRoutes.get('/', function(req, res) {
			res.json({ message: config.app_name });
	});



	  
	var fs = require('fs');
	var files = fs.readdirSync('./controllers');

	console.log(files)

		for (x in files)
		 if(!files[x].match('gitignore|base|Base|config|zip|json|all|Socket|save') && files[x].match('.js'))				 
   		      require('./' + files[x])(app, apiRoutes, io);

}