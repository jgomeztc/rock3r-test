
// Module Controller 

module.exports = function(app, privateRoutes, io){
  
    var mongoose = require('mongoose');
    var Model = require('../models/Base');

    //this will deploy de basic endpoints

    var entity_name = "instagram";


    function get(req, res){

      var REQ = req.params; 

       Model
       .find()
       .populate("_route")
       .exec(function(err, rs){

           if(!err)
           {
            
            res.json(rs);

           }
           else
            res.json(err);


       });

    }


    function post(req, res){
    
     var data = {};
     var REQ = req.body || req.params;

     console.log(REQ)

     !REQ.data || (data.data = REQ.data);             
     !REQ._route || (data._route = mongoose.Types.ObjectId(REQ._route));       

     var model = new Model(data);
      
     model.save(function(err, rs){
        if(rs)
          res.json(rs);
     });

    }


    function update(req, res){
    
     var data = {};
     var REQ = req.body || req.params;

     console.log(REQ)

     !REQ.data || (data.data = REQ.data);             
     !REQ._route || (data._route = mongoose.Types.ObjectId(REQ._route));   

     data = { $set : data };          

     Model.update({ _id : mongoose.Types.ObjectId(req.params.id) }, data,function(err, rs){

      if(rs)
        res.json(rs);

     });

    }

    function asociate_passengers(req, res){

        var REQ = req.body || req.params;

        console.log(REQ.services, "request array")

        Model.asociate_passengers(req.params.id, REQ.services, function(err, module){

              if(module)
              {
                res.json(module);
              }
              else
                res.json(err);

        })

    }

  function remove(req, res){

        Model.remove({_id : mongoose.Types.ObjectId(req.params.id)}, function(err, rs){
           
              if(!err)
                  res.json(rs);
              else
                 res.status(500).json(err);

        });

    }

    //basic endpoints



    
    //this is the public endpoint
    app.get("/" + entity_name, get);

    //this is the private endpoints
    privateRoutes.get("/api/" + entity_name, get);
    privateRoutes.post("/" + entity_name, post);
    privateRoutes.put("/" + entity_name + "/:id", update);
    privateRoutes.delete("/" + entity_name + "/:id", remove);


    //aditional endpoints


    // this will allow to us get token from an instagram account and store it for future use.

    function authorize(){

    }


    //this will handle what we need from a post

    function getPostFeatures(){

    }


    // ex. privateRoutes.delete("/" + entity_name + "/custom/path", remove);


    return this;

}