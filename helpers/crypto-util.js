var crypto = require('crypto');
var config = require('../config.js');

var crypto_util = {
	myHash : function (msg) {
    	return crypto.createHash('sha256').update(msg).digest('hex');
	},

	randomHex : function (len) {
    	return crypto.randomBytes(Math.ceil(len/2))
        		.toString('hex')
        		.slice(0, len);   
	},

	randomValueBase64 : function(len){
	    return crypto.randomBytes(Math.ceil(len * 3 / 4))
	        .toString('base64')   // convert to base64 format
	        .slice(0, len)        // return required number of characters
	        .replace(/\+/g, '0')  // replace '+' with '0'
	        .replace(/\//g, '0'); // replace '/' with '0'
	},

	randomCharset : function (howMany, chars) {
		    chars = chars || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		    var rnd = crypto.randomBytes(howMany)
		        , value = new Array(howMany)
		        , len = chars.length;

		    for (var i = 0; i < howMany; i++) {
		        value[i] = chars[rnd[i] % len]
		    };

		    return value.join('');
	}
}

module.exports = crypto_util;