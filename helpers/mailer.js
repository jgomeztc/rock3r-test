module.exports = {
	send : function(template, options, bind, callback) {
			
			var ejs = require('ejs');
			var fs = require('fs');
			var path = require('path');
			var nodemailer = require('nodemailer');
			var config = ('../config.js');
			
			var transporter = nodemailer.createTransport(
				'smtps://'
				+ config.smtp_email  
				+ ':' 
				+ config.smtp_password
				+ '@'
				+ config.smtp_server 
				);
			
			var mailOptions = {
			    from: options.FROM,
			    to: options.TO,
			    subject: options.SUBJECT
			};

			var template = path.join(process.env.PWD, 'email_templates', template);

			fs.readFile(template, 'utf-8', function(_err, content){
				if(_err){ throw _err }
					var _template = ejs.render(content, bind);
					mailOptions.html = _template;
					transporter.sendMail(mailOptions, callback);
			});
	}
}