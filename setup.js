module.exports = {
	  roles : [
	  	{name : "admin", privileges : "admin::admin"},
	  	{name : "monitor admin", privileges : "monitor::admin"},
	  	{name : "monitor", privileges : "monitor::monitor"},
	  	{name : "student", privileges : "student::student"},
	  	{name : "admin", privileges : "admin::admin"},
	  	{name : "super", privileges : "all::all"}
	  ],
	  user : {
		  username : "batman",
		  password : '123456',
		  name : "batman",
		  last_name : "raise",
		  email : "sales@batman.com"
	}
}